Сменить права на директорию/файл

`Chmod 777 filename`



Изменить дату создания/доступа к файлу

`touch -amt [YYYY]MMDDHHmm filename` 

(a - accessed, m - modified, t - timestamp)



`-rwx--xr--  1 user  2019    0 Jun  1 21:46 test1`

`d` - признак директории

`rex--xr--` права доступа

`1` количество жестких ссылок



Создание жесткой ссылки на файл

`ln sourcefile destfile`



Создание мягкой ссылки на файл

`ln -s sourcefile destfile`



Создание файла заданного размера

`mkfile 100500 filename`



Найти файл

`find . -type f -name "*~"`



Найти файл по нескольким шаблонам

`find . -type f \(- name "abc*" - o -name "*def" \)`



Найти и удалить

`find . -name "foo" -print -delete`



Замена с sed

`sed 's/REGEX/REPLACEMENT/FLAGS'`

s - substitute



